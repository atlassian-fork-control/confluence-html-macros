package com.atlassian.confluence.plugins.macros.html;

import com.atlassian.applinks.api.ReadOnlyApplicationLinkService;
import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.RenderedContentCleaner;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.plugin.services.VelocityHelperService;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugins.whitelist.OutboundWhitelist;
import com.atlassian.renderer.v2.RenderUtils;
import com.atlassian.sal.api.net.NonMarshallingRequestFactory;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import com.google.common.annotations.VisibleForTesting;
import com.opensymphony.util.TextUtils;
import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.io.FeedException;
import com.sun.syndication.io.SyndFeedInput;
import com.sun.syndication.io.XmlReader;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static java.util.Collections.singletonList;

public class RssMacro extends WhitelistedHttpRetrievalMacro
{
    private static final Logger log = LoggerFactory.getLogger(RssMacro.class);
    private static final List<String> SCHEMAS = Arrays.asList("http", "https");

    private final VelocityHelperService velocityHelperService;
    private final RenderedContentCleaner contentCleaner;

    @Autowired
    public RssMacro(
            @ComponentImport LocaleManager localeManager,
            @ComponentImport I18NBeanFactory i18NBeanFactory,
            @ComponentImport NonMarshallingRequestFactory<Request<?, Response>> requestFactory,
            @ComponentImport ReadOnlyApplicationLinkService applicationLinkService,
            @ComponentImport VelocityHelperService velocityHelperService,
            @ComponentImport OutboundWhitelist whitelist,
            @ComponentImport RenderedContentCleaner contentCleaner) {
        super(localeManager, i18NBeanFactory, requestFactory, applicationLinkService, whitelist);
        this.velocityHelperService = velocityHelperService;
        this.contentCleaner = contentCleaner;
    }

    @Override
    protected String successfulResponse(Map<String, String> parameters, ConversionContext conversionContext, String url, Response response) throws MacroExecutionException {
        int maxItems = TextUtils.parseInt(RenderUtils.getParameter(parameters, "max", 1));
        String titleBar = TextUtils.noNull(RenderUtils.getParameter(parameters, "titleBar", 2)).trim();
        boolean showTitlesOnly = TextUtils.parseBoolean(RenderUtils.getParameter(parameters, "showTitlesOnly", 2));

        SyndFeed feed;
        try {
            feed = parseRSSFeed(url, IOUtils.toByteArray(response.getResponseBodyAsStream()));
        } catch (IOException | ResponseException e) {
            // Don't escape e.toString() because it should be interpreted as HTML
            throw new MacroExecutionException(getText("rss.error.parse", singletonList(e.toString())), e);
        }

        Map<String, Object> contextMap = getMacroVelocityContext();
        contextMap.put("url", url);
        contextMap.put("feed", feed);
        contextMap.put("max", maxItems);
        contextMap.put("showTitlesOnly", showTitlesOnly);
        contextMap.put("contentCleaner", contentCleaner);
        contextMap.put("titleBar", Boolean.toString(!titleBar.equalsIgnoreCase("false") && !titleBar.equalsIgnoreCase("no")));

        try {
            verifyLinksInFeed(feed);
            return renderRssFeeds(contextMap);
        }
        catch (Exception e) {
            log.error("Error while trying to assemble the RSS result!", e);
            throw new MacroExecutionException(e.getMessage());
        }
    }

    @VisibleForTesting
    Map<String, Object> getMacroVelocityContext() {
        return velocityHelperService.createDefaultVelocityContext();
    }

    @VisibleForTesting
    String renderRssFeeds(Map<String, Object> contextMap) {
        return velocityHelperService.getRenderedTemplate("com/atlassian/confluence/plugins/macros/html/rss.vm", contextMap);
    }

    private void verifyLinksInFeed(final SyndFeed feed) throws FeedException, URISyntaxException {
        if (feed == null) {
            return;
        }
        verifyLink(feed.getLink());
        for (Object entry : feed.getEntries()) {
            verifyLink(((SyndEntry) entry).getLink());
        }
    }

    private void verifyLink(final String link) throws FeedException, URISyntaxException {
        if (link == null) {
            return;
        }
        final URI uri = new URI(link);
        if (SCHEMAS.stream().noneMatch(schema -> schema.equalsIgnoreCase(uri.getScheme()))) {
            throw new FeedException("Unsupported schema found in RSS feed");
        }
    }

    private SyndFeed parseRSSFeed(String url, byte[] webContent) throws IOException {
        try (ByteArrayInputStream bufferedIn = new ByteArrayInputStream(webContent)) {
            try {
                SyndFeedInput input = new SyndFeedInput();
                return input.build(new XmlReader(bufferedIn));
            } catch (IOException ioe) {
                throw new FeedException("Unable to read XML from " + url, ioe);
            }
        } catch (FeedException e) {
            log.error("Error while trying to assemble the RSS result! Url: " + url, e);
            throw new IOException("Unable to parse rss feed from [" + url + "] due to " + e.getMessage());
        }
    }
}
