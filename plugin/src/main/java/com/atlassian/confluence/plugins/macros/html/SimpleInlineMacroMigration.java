package com.atlassian.confluence.plugins.macros.html;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.definition.PlainTextMacroBody;
import com.atlassian.confluence.macro.xhtml.MacroMigration;
import com.atlassian.confluence.xhtml.api.MacroDefinition;
import org.apache.commons.lang3.StringUtils;

public class SimpleInlineMacroMigration implements MacroMigration
{
    @Override
    public MacroDefinition migrate(MacroDefinition macroDefinition, ConversionContext conversionContext)
    {
        macroDefinition.setBody(new PlainTextMacroBody(StringUtils.defaultString(macroDefinition.getBodyText())));
        return macroDefinition;
    }
}
