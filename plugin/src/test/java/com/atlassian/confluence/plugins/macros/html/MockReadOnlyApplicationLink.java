package com.atlassian.confluence.plugins.macros.html;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.applinks.api.ApplicationType;
import com.atlassian.applinks.api.ReadOnlyApplicationLink;
import com.atlassian.applinks.api.auth.AuthenticationProvider;

import java.net.URI;

/**
 * @since 4.0.0
 */
public class MockReadOnlyApplicationLink implements ReadOnlyApplicationLink {
    private final String url;
    private final ApplicationLinkRequestFactory applicationLinkRequestFactory;

    MockReadOnlyApplicationLink(String url, ApplicationLinkRequestFactory applicationLinkRequestFactory) {
        this.url = url;
        this.applicationLinkRequestFactory = applicationLinkRequestFactory;
    }

    @Override
    public ApplicationId getId() {
        return null;
    }

    @Override
    public ApplicationType getType() {
        return null;
    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public URI getDisplayUrl() {
        return URI.create(url);
    }

    @Override
    public URI getRpcUrl() {
        return null;
    }

    @Override
    public boolean isPrimary() {
        return false;
    }

    @Override
    public boolean isSystem() {
        return false;
    }

    @Override
    public ApplicationLinkRequestFactory createAuthenticatedRequestFactory() {
        return applicationLinkRequestFactory;
    }

    @Override
    public ApplicationLinkRequestFactory createAuthenticatedRequestFactory(Class<? extends AuthenticationProvider> providerClass) {
        return null;
    }

    @Override
    public ApplicationLinkRequestFactory createImpersonatingAuthenticatedRequestFactory() {
        return null;
    }

    @Override
    public ApplicationLinkRequestFactory createNonImpersonatingAuthenticatedRequestFactory() {
        return null;
    }
}
