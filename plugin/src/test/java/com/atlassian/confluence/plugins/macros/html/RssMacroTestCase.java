package com.atlassian.confluence.plugins.macros.html;

import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.applinks.api.ReadOnlyApplicationLink;
import com.atlassian.confluence.content.render.xhtml.RenderedContentCleaner;
import com.atlassian.confluence.plugin.services.VelocityHelperService;
import com.atlassian.renderer.v2.macro.MacroException;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.api.net.ResponseHandler;
import com.sun.syndication.feed.synd.SyndFeedImpl;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.stubbing.Answer;

import javax.annotation.Nonnull;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class RssMacroTestCase extends WhitelistedHttpRetrievalMacroTestCase<RssMacro> {
    @Mock
    private VelocityHelperService velocityHelperService;

    @Mock
    private RenderedContentCleaner contentCleaner;

    protected RssMacro createMacro() {
        return new RssMacro(localeManager, i18NBeanFactory, requestFactory, applicationLinkService, velocityHelperService, whitelist, contentCleaner);
    }

    /**
     * Regular case, successfully retrieved and parsed rss feed
     */
    @Test
    public void testRssXmlReadUsingXmlReader() throws MacroException, IOException, ResponseException, CredentialsRequiredException {
        String url = "http://localhost:1992/jira/sr/jira.issueviews:searchrequest-rss/temp/SearchRequest.xml?pid=10000&sorter/field=issuekey&sorter/order=DESC&tempMax=1000";
        final String rss = getClasspathResourceAsString("com/atlassian/confluence/plugins/macros/html/htmlmacros-20.xml");

        macroParameters.put("url", url);

        final List<ReadOnlyApplicationLink> applicationLinks = singletonList(new MockReadOnlyApplicationLink("http://localhost:1992", applicationLinkRequestFactory));

        when(applicationLinkService.getApplicationLinks()).thenReturn(new Iterable<ReadOnlyApplicationLink>() {
            @Override
            @Nonnull
            public Iterator<ReadOnlyApplicationLink> iterator() {
                return applicationLinks.iterator();
            }
        });

        when(response.getStatusCode()).thenReturn(204);
        when(response.getResponseBodyAsStream()).thenReturn(new ByteArrayInputStream(rss.getBytes()));

        when(applicationLinkRequestFactory.createRequest(any(), any())).thenReturn(applicationLinkRequest);

        doAnswer((Answer<Void>) invocation -> {
            Map<String, Object> contextMap = (Map<String, Object>) invocation.getArguments()[1];
            SyndFeedImpl syndFeed = (SyndFeedImpl) contextMap.get("feed");

            // The test rss contains 10 entries. If we get all of them here, it means rss parsing is working properly
            // No need to check other details because it is the responsibility of Rome library
            assertEquals(10, syndFeed.getEntries().size());
            return null;
        }).when(velocityHelperService).getRenderedTemplate(any(), any());

        doAnswer((Answer<Void>) invocation -> {
            ResponseHandler handler = (ResponseHandler) invocation.getArguments()[0];
            handler.handle(response);
            return null;
        }).when(applicationLinkRequest).execute(any(ResponseHandler.class));

        final RssMacro testRssMacro = createMacro();
        testRssMacro.execute(macroParameters, null, pageToBeRendered.toPageContext());
        verify(requestFactory, times(0)).createRequest(any(), any());
        verify(applicationLinkRequest, times(1)).execute(any(ResponseHandler.class));
    }

    @Test(expected = MacroException.class)
    public void testRssFeedHavingXSS() throws MacroException, IOException, ResponseException, CredentialsRequiredException {
        String url = "http://localhost:1992/jira/sr/jira.issueviews:searchrequest-rss/temp/SearchRequest.xml?pid=10000&sorter/field=issuekey&sorter/order=DESC&tempMax=1000";
        final String theRssWithXss = getClasspathResourceAsString("com/atlassian/confluence/plugins/macros/html/53916.xss.xml");

        macroParameters.put("url", url);

        final List<ReadOnlyApplicationLink> applicationLinks = singletonList(new MockReadOnlyApplicationLink("http://localhost:1992", applicationLinkRequestFactory));

        when(applicationLinkService.getApplicationLinks()).thenReturn(new Iterable<ReadOnlyApplicationLink>() {
            @Override
            @Nonnull
            public Iterator<ReadOnlyApplicationLink> iterator() {
                return applicationLinks.iterator();
            }
        });

        when(response.getStatusCode()).thenReturn(204);
        when(response.getResponseBodyAsStream()).thenReturn(new ByteArrayInputStream(theRssWithXss.getBytes()));

        when(applicationLinkRequestFactory.createRequest(any(), any())).thenReturn(applicationLinkRequest);

        doAnswer((Answer<Void>) invocation -> {
            ResponseHandler handler = (ResponseHandler) invocation.getArguments()[0];
            handler.handle(response);
            return null;
        }).when(applicationLinkRequest).execute(any(ResponseHandler.class));

        final RssMacro testRssMacro = createMacro();
        // MacroException should be thrown here
        testRssMacro.execute(macroParameters, null, pageToBeRendered.toPageContext());
    }
}
