package it.com.atlassian.confluence.plugins.macros.html;

public class RssMacroWhitelistTestCase extends AbstractConfluencePluginWebTestCaseBase {
    private static final String MACRO_NAME = "rss";
    private static final String REMOTE_RESOURCE = "/download/attachments/950274/rssfeed.xml";

    /**
     * The id of the page created to embed the RSS macro in.
     */
    private long testPageId;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        ctkServerResourceModifier.setXmlFromFile(REMOTE_RESOURCE, "/com/atlassian/confluence/plugins/macros/html/rssfeed.xml");
        testPageId = createPage("RssMacroWhitelistTestCase", createPageContent());
    }

    public void testWhitelistDenyWithoutAnyRule() {
        assertRemoteContentBlocked();
    }

    public void testWhitelistAllowUsingWildcard() {
        assertRemoteContentBlocked();
        whitelistTestRule.whitelistWildcard(remoteBaseUrl + "/*");
        assertRemoteContentIncluded();
    }

    public void testWhitelistAllowUsingExactMatch() {
        assertRemoteContentBlocked();
        whitelistTestRule.whitelistExactUrl(remoteBaseUrl + REMOTE_RESOURCE);
        assertRemoteContentIncluded();
    }

    public void testWhitelistAllowUsingRegex() {
        assertRemoteContentBlocked();
        whitelistTestRule.whitelistRegularExpression(remoteBaseUrl + "/download/attachments/[\\d]+/rssfeed.xml");
        assertRemoteContentIncluded();
    }

    private void assertRemoteContentIncluded() {
        viewPageById(testPageId);
        assertRssFeedShown();
    }

    private void assertRemoteContentBlocked() {
        viewPageById(testPageId);
        assertRssFeedNotShown();
    }

    private void assertRssFeedNotShown() {
        assertElementNotPresentByXPath("//div[@class='rssMacro']//tr[1]//a[text()='\"Test RSS Exploit showing\"']");
        assertEquals("Could not access the content at the URL because it is not from an allowed source.",
                getElementTextByXPath("//div[@class='wiki-content']//div[contains(@class,'errorBox')]//p//strong"));
        assertLinkPresentWithText("Configure whitelist >>");
    }

    private void assertRssFeedShown() {
        assertElementPresentByXPath("//div[contains(@class,'rssMacro')]//tr[1]//a[text()='\"Test RSS Exploit showing\"']");
        assertEquals("This is description & test",
                getElementTextByXPath("//div[contains(@class,'rssMacro')]//tr[1]//span"));
        assertElementPresentByXPath("//div[contains(@class,'rssMacro')]//tr[2]//a[text()='Items & Test']");
        assertEquals("Item description includes html markup",
                getElementTextByXPath("//div[contains(@class,'rssMacro')]//tr[2]//span"));
    }

    private String createPageContent() {
        return "{" + MACRO_NAME + ":url=" + remoteBaseUrl + REMOTE_RESOURCE + "}";
    }
}
