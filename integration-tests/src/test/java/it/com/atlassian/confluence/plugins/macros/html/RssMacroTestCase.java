package it.com.atlassian.confluence.plugins.macros.html;

import java.io.File;
import java.io.IOException;

import com.atlassian.confluence.plugin.functest.ConfluenceWebTester;
import com.atlassian.confluence.plugin.functest.helper.AttachmentHelper;
import com.atlassian.confluence.plugin.functest.helper.PageHelper;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;

public class RssMacroTestCase extends AbstractConfluencePluginWebTestCaseBase
{

    //https://jira.atlassian.com/browse/CONFDEV-31857
    public void testNoop()
    {

    }
    /**
     * <a href="http://developer.atlassian.com/jira/browse/HTMLMACROS-11">HTMLMACROS-11</a>
     */
    //https://jira.atlassian.com/browse/CONFDEV-31857
    public void _testEntityReferencesNotExpanded() throws IOException
    {
        flushCaches();
        whitelistTestRule.disableWhitelist();

        PageHelper helper = getPageHelper();

        helper.setSpaceKey(TEST_SPACE_KEY);
        helper.setTitle("testEntityReferencesNotExpanded"); /* Avoid Confluence caching problem */
        assertTrue(helper.create());

        File tempFile = File.createTempFile("foo", StringUtils.EMPTY);

        try
        {
            String fileContent = "HTMLMACROS-11";

            FileUtils.writeStringToFile(tempFile, fileContent);

            String rssContentString = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?>\n" +
                    "<!DOCTYPE foo [\n" +
                    "<!ENTITY " + tempFile.getName() + " SYSTEM \"" + tempFile.toURI() + "\">\n" +
                    "]>\n" +
                    "<rss version=\"2.0\">\n" +
                    "<channel>\n" +
                    "<title>&quot;Test RSS Exploit showing&quot; " + tempFile.toURI() + "</title>\n" +
                    "<description>this is " + tempFile.toURI() + ": &" + tempFile.getName() + ";</description>\n" +
                    "<item>\n" +
                    "<title>" + tempFile.getAbsolutePath() + "</title>\n" +
                    "<description>" + tempFile.toURI() + ": &" + tempFile.getName() + ";</description>\n" +
                    "<link>https://www.atlassian.com</link>\n" +
                    "</item>\n" +
                    "</channel>\n" +
                    "</rss>";
            byte[] rssContent = rssContentString.getBytes("UTF-8");

            AttachmentHelper attachment = getAttachmentHelper();

            attachment.setParentId(helper.getId());
            attachment.setFileName(RandomStringUtils.randomAlphanumeric(8));
            attachment.setContentType("text/plain");
            attachment.setContent(rssContent);
            attachment.setContentLength(rssContent.length);

            assertTrue(attachment.create());

            ConfluenceWebTester confluenceWebTester = getConfluenceWebTester();
            String baseUrl = confluenceWebTester.getBaseUrl();

            helper.setContent(
                    "{rss:url="
                            + baseUrl
                            + "/download/attachments/"
                            + helper.getId()
                            + "/"
                            + attachment.getFileName()
                            + "?os_username=" + confluenceWebTester.getCurrentUserName()
                            + "&os_password=" + confluenceWebTester.getCurrentPassword()
                            + "}");
            assertTrue(helper.update());

            getIndexHelper().update();

            gotoPage("/pages/viewpage.action?pageId=" + helper.getId());

            assertTextNotPresent(fileContent);
            assertTextPresent("\"Test RSS Exploit showing\""); /* Make sure that normal entities are processed. */
            assertElementPresentByXPath("//div[@class='rssMacro']");
        }
        finally
        {
            if (null != tempFile)
            tempFile.delete();
        }
    }
}
